package at.petritzdesigns.bitbot.control.pi2go;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Pi2GoRoboterTest {

    public Pi2GoRoboterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setup method, of class Pi2GoRoboter.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testSetup() throws Exception {
        System.out.println("setup");
        Pi2GoRoboter instance = new Pi2GoRoboter();
        boolean expResult = true;
        boolean result = instance.setup();
        assertEquals(expResult, result);
    }

    /**
     * Test of shutdown method, of class Pi2GoRoboter.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testShutdown() throws Exception {
        System.out.println("shutdown");
        Pi2GoRoboter instance = new Pi2GoRoboter();
        boolean expResult = true;
        boolean result = instance.shutdown();
        assertEquals(expResult, result);
    }

    /**
     * Test of moveLeft method, of class Pi2GoRoboter.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testMoveLeft() throws Exception {
        System.out.println("moveLeft");
        int speed = 0;
        Pi2GoRoboter instance = new Pi2GoRoboter();
        boolean expResult = true;
        boolean result = instance.getLeftMotor().move(speed);
        assertEquals(expResult, result);
    }

    /**
     * Test of moveRight method, of class Pi2GoRoboter.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testMoveRight() throws Exception {
        System.out.println("moveRight");
        int speed = 0;
        Pi2GoRoboter instance = new Pi2GoRoboter();
        boolean expResult = true;
        boolean result = instance.getRightMotor().move(speed);
        assertEquals(expResult, result);
    }

}
