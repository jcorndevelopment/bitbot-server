package at.petritzdesigns.bitbot.control.pi2go;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.control.components.IRSensor;
import at.petritzdesigns.bitbot.control.components.Led;
import at.petritzdesigns.bitbot.control.components.Motor;
import at.petritzdesigns.bitbot.control.components.Servo;
import at.petritzdesigns.bitbot.control.components.Switch;
import at.petritzdesigns.bitbot.control.components.UltraSoundSensor;
import at.petritzdesigns.bitbot.control.pi2go.components.Hcsr04SinglePin;
import at.petritzdesigns.bitbot.control.pi2go.components.Pi2GoIRSensor;
import at.petritzdesigns.bitbot.control.pi2go.components.Pi2GoLed;
import at.petritzdesigns.bitbot.control.pi2go.components.Pi2GoMotor;
import at.petritzdesigns.bitbot.control.pi2go.components.Pi2GoServo;
import at.petritzdesigns.bitbot.control.pi2go.components.Pi2GoSwitch;
import at.petritzdesigns.bitbot.exceptions.BitBotControlException;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.wiringpi.Gpio;
import java.io.IOException;

/**
 * The hardware implementation of the Pi2Go roboter. It also includes all the
 * hardware components available and the confoguration of the pins.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Pi2GoRoboter implements Roboter {

    private final Pin SWITCH_PIN = RaspiPin.GPIO_14;

    private final Pin LEFT_IR_SENSOR_PIN = RaspiPin.GPIO_07;
    private final Pin RIGHT_IR_SENSOR_PIN = RaspiPin.GPIO_00;
    private final Pin LINE_LEFT_IRSENSOR_PIN = RaspiPin.GPIO_02;
    private final Pin LINE_RIGHT_IRSENSOR_PIN = RaspiPin.GPIO_01;

    private final Pin FRONT_LEDS_PIN = RaspiPin.GPIO_03;
    private final Pin REAR_LEDS_PIN = RaspiPin.GPIO_04;

    private final Pin ULTRA_SOUND_SENSOR_PIN = RaspiPin.GPIO_15;

    private final Pin FORWARD_RIGHT_MOTOR_PIN = RaspiPin.GPIO_12;
    private final Pin BACKWARD_RIGHT_MOTOR_PIN = RaspiPin.GPIO_13;
    private final Pin FORWARD_LEFT_MOTOR_PIN = RaspiPin.GPIO_11;
    private final Pin BACKWARD_LEFT_MOTOR_PIN = RaspiPin.GPIO_10;

    private final Pin PAN_SERVO_PIN = RaspiPin.GPIO_05;
    private final Pin TILT_SERVO_PIN = RaspiPin.GPIO_06;

    private final Motor leftMotor;
    private final Motor rightMotor;
    private final UltraSoundSensor ultraSoundSensor;
    private final Switch button;
    private final Led frontLeds;
    private final Led rearLeds;
    private final IRSensor lineLeftIRSensor;
    private final IRSensor lineRightIRSensor;
    private final IRSensor leftIRSensor;
    private final IRSensor rightIRSensor;
    private final Servo panServo;
    private final Servo tiltServo;

    private boolean needsSetup = true;

    public Pi2GoRoboter() throws IOException {
        leftMotor = new Pi2GoMotor(FORWARD_LEFT_MOTOR_PIN, BACKWARD_LEFT_MOTOR_PIN);
        rightMotor = new Pi2GoMotor(FORWARD_RIGHT_MOTOR_PIN, BACKWARD_RIGHT_MOTOR_PIN);
        ultraSoundSensor = new Hcsr04SinglePin(ULTRA_SOUND_SENSOR_PIN);
        button = new Pi2GoSwitch(SWITCH_PIN);
        frontLeds = new Pi2GoLed(FRONT_LEDS_PIN, false);
        rearLeds = new Pi2GoLed(REAR_LEDS_PIN, false);
        lineLeftIRSensor = new Pi2GoIRSensor(LINE_LEFT_IRSENSOR_PIN);
        lineRightIRSensor = new Pi2GoIRSensor(LINE_RIGHT_IRSENSOR_PIN);
        leftIRSensor = new Pi2GoIRSensor(LEFT_IR_SENSOR_PIN);
        rightIRSensor = new Pi2GoIRSensor(RIGHT_IR_SENSOR_PIN);
        panServo = new Pi2GoServo(PAN_SERVO_PIN);
        tiltServo = new Pi2GoServo(TILT_SERVO_PIN);
    }

    /**
     * Setup wiringpi library and create soft PWM
     *
     * @return success
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean setup() throws BitBotControlException {
        if (needsSetup) {
            int ret = Gpio.wiringPiSetup();
            if (ret != 0) {
                return false;
            }
            leftMotor.setup();
            rightMotor.setup();
            needsSetup = false;
            return true;
        }
        return true;
    }

    /**
     * Sets motor speed to 0
     *
     * @return success
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean shutdown() throws BitBotControlException {
        leftMotor.shutdown();
        rightMotor.shutdown();
        frontLeds.turnOff();
        rearLeds.turnOff();
        button.shutdown();
        return true;
    }

    @Override
    public Motor getLeftMotor() throws BitBotControlException {
        return leftMotor;
    }

    @Override
    public Motor getRightMotor() throws BitBotControlException {
        return rightMotor;
    }

    @Override
    public UltraSoundSensor getUltraSoundSensor() {
        return ultraSoundSensor;
    }

    @Override
    public Switch getSwitch() {
        return button;
    }

    @Override
    public Led getFrontLeds() {
        return frontLeds;
    }

    @Override
    public Led getRearLeds() {
        return rearLeds;
    }

    @Override
    public IRSensor getLineLeftIRSensor() {
        return lineLeftIRSensor;
    }

    @Override
    public IRSensor getLineRightIRSensor() {
        return lineRightIRSensor;
    }

    @Override
    public IRSensor getLeftIRSensor() {
        return leftIRSensor;
    }

    @Override
    public IRSensor getRightIRSensor() {
        return rightIRSensor;
    }

    @Override
    public Servo getPanServo() {
        return panServo;
    }

    @Override
    public Servo getTiltServo() {
        return tiltServo;
    }
}
