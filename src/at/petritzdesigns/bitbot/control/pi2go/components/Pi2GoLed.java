package at.petritzdesigns.bitbot.control.pi2go.components;

import at.petritzdesigns.bitbot.control.components.Led;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

/**
 * The hardware implementation of the LED.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class Pi2GoLed implements Led {

    private final GpioPinDigitalOutput led;
    private final PinState on;
    private final PinState off;

    public Pi2GoLed(Pin ledPin, boolean onIsHigh) {
        if (onIsHigh) {
            this.on = PinState.HIGH;
            this.off = PinState.LOW;
        } else {
            this.on = PinState.LOW;
            this.off = PinState.HIGH;
        }

        this.led = GpioFactory.getInstance().provisionDigitalOutputPin(ledPin, off);
        this.led.setShutdownOptions(true, off);
    }

    @Override
    public void pulse(long duration) {
        led.pulse(duration, on, false);
    }

    @Override
    public void toggle() {
        led.toggle();
    }

    @Override
    public void turnOff() {
        led.setState(on);
    }

    @Override
    public void turnOn() {
        led.setState(off);
    }

}
