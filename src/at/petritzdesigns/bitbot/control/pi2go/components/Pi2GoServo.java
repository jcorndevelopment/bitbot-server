package at.petritzdesigns.bitbot.control.pi2go.components;

import at.petritzdesigns.bitbot.control.components.Servo;
import com.pi4j.component.servo.ServoDriver;
import com.pi4j.component.servo.ServoProvider;
import com.pi4j.component.servo.impl.RPIServoBlasterProvider;
import com.pi4j.io.gpio.Pin;
import java.io.IOException;

/**
 * The hardware implementation of the Servo motor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class Pi2GoServo implements Servo {

    private final ServoProvider servoProvider;
    private final ServoDriver servo;

    public Pi2GoServo(Pin servoPin) throws IOException {
        this.servoProvider = new RPIServoBlasterProvider();
        this.servo = servoProvider.getServoDriver(servoPin);
    }

    @Override
    public void setDegree(int degrees) {
        servo.setServoPulseWidth(50 + ((90 - degrees) * 200 / 180));
    }

    @Override
    public void setPulseWidth(int width) {
        servo.setServoPulseWidth(width);
    }

}
