package at.petritzdesigns.bitbot.control.pi2go.components;

import at.petritzdesigns.bitbot.control.components.UltraSoundSensor;
import com.pi4j.io.gpio.Pin;
import com.pi4j.wiringpi.Gpio;

/**
 * The hardware implementation of the ultra sonic sensor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class Hcsr04SinglePin implements UltraSoundSensor {

    private final Pin pin;

    public Hcsr04SinglePin(Pin pin) {
        this.pin = pin;
    }

    /**
     * Get the distance in mm
     *
     * @return distance in mm
     */
    @Override
    public long getDistance() {
        long start = 0;
        long end = 0;

        long loopStart;

        Gpio.pinMode(pin.getAddress(), Gpio.OUTPUT);
        Gpio.digitalWrite(pin.getAddress(), false);
        Gpio.delayMicroseconds(2);
        Gpio.digitalWrite(pin.getAddress(), true);
        Gpio.delayMicroseconds(10);
        Gpio.digitalWrite(pin.getAddress(), false);
        Gpio.delayMicroseconds(2);
        Gpio.pinMode(pin.getAddress(), Gpio.INPUT);

        loopStart = Gpio.micros();
        // Wait for the signal to return
        while (Gpio.digitalRead(pin.getAddress()) == 0 && (Gpio.micros() - loopStart) < 500000) {
            start = Gpio.micros();
        }

        loopStart = Gpio.micros();
        while (Gpio.digitalRead(pin.getAddress()) == 1 && (Gpio.micros() - loopStart) < 500000) {
            end = Gpio.micros();
        }

        if (start == 00 || end == 0) {
            System.out.println(" ==>> " + start + " " + end);
            return start;
        }

        long distance = ((end - start) * 17190 / 100000);
        return distance;
    }

}
