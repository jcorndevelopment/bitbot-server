package at.petritzdesigns.bitbot.control.pi2go.components;

import at.petritzdesigns.bitbot.control.components.Motor;
import at.petritzdesigns.bitbot.exceptions.BitBotControlException;
import com.pi4j.io.gpio.Pin;
import com.pi4j.wiringpi.SoftPwm;

/**
 * The hardware implementation of the DC motor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class Pi2GoMotor implements Motor {

    private final Pin forwardPin;
    private final Pin backwardPin;

    public Pi2GoMotor(Pin forwardPin, Pin backwardPin) {
        this.forwardPin = forwardPin;
        this.backwardPin = backwardPin;
    }

    @Override
    public boolean setup() throws BitBotControlException {
        SoftPwm.softPwmCreate(forwardPin.getAddress(), 0, 100);
        SoftPwm.softPwmCreate(backwardPin.getAddress(), 0, 100);
        return true;
    }

    @Override
    public boolean move(int speed) throws BitBotControlException {
        if (speed < -100 || speed > 100) {
            throw new BitBotControlException("Speed is not in range. Should be -100 to 100");
        }
        if (speed >= 0) {
            SoftPwm.softPwmWrite(forwardPin.getAddress(), +speed);
            SoftPwm.softPwmWrite(backwardPin.getAddress(), 0);
        } else {
            SoftPwm.softPwmWrite(forwardPin.getAddress(), 0);
            SoftPwm.softPwmWrite(backwardPin.getAddress(), -speed);
        }
        return true;
    }

    @Override
    public boolean shutdown() throws BitBotControlException {
        SoftPwm.softPwmWrite(forwardPin.getAddress(), 0);
        SoftPwm.softPwmWrite(backwardPin.getAddress(), 0);
        return true;
    }

}
