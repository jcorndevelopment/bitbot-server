package at.petritzdesigns.bitbot.control.pi2go.components;

import at.petritzdesigns.bitbot.control.components.IRSensor;
import at.petritzdesigns.bitbot.control.listener.IRSensorListener;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import java.util.ArrayList;
import java.util.List;

/**
 * The hardware implementation of the infrared sensor.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class Pi2GoIRSensor implements IRSensor {

    private final List<IRSensorListener> listeners;
    private final GpioPinDigitalInput irSensor;

    public Pi2GoIRSensor(Pin pin) {
        listeners = new ArrayList<>();
        irSensor = GpioFactory.getInstance()
                .provisionDigitalInputPin(pin, PinPullResistance.PULL_DOWN);
        irSensor.setShutdownOptions(Boolean.TRUE);

        irSensor.addListener((GpioPinListenerDigital) (GpioPinDigitalStateChangeEvent event) -> {
            listeners.stream().forEach((listener) -> {
                listener.triggered(event.getState().isLow());
            });
        });
    }

    private List<IRSensorListener> getListener() {
        return listeners;
    }

    @Override
    public boolean isTriggered() {
        return irSensor.isHigh();
    }

    @Override
    public void addListener(IRSensorListener listener) {
        getListener().add(listener);
    }

    @Override
    public void removeListener(IRSensorListener listener) {
        getListener().remove(listener);
    }

}
