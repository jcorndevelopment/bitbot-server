package at.petritzdesigns.bitbot.control.pi2go.components;

import at.petritzdesigns.bitbot.control.components.Switch;
import at.petritzdesigns.bitbot.control.listener.SwitchListener;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import java.util.ArrayList;
import java.util.List;

/**
 * The hwardware implementation of the switch/button.
 *
 * @version 1.0.0
 * @author Markus Petritz
 */
public class Pi2GoSwitch implements Switch {

    private List<SwitchListener> listeners;
    private final GpioPinDigitalInput button;
    private long lastLow;
    private long lastHigh;

    public Pi2GoSwitch(Pin switchPin) {
        listeners = new ArrayList<>();
        button = GpioFactory.getInstance()
                .provisionDigitalInputPin(switchPin, PinPullResistance.PULL_UP);
        button.addListener((GpioPinListenerDigital) (GpioPinDigitalStateChangeEvent event) -> {
            synchronized (this) {
                if (event.getState().isLow()) {
                    lastLow = System.currentTimeMillis();
                }
                if (event.getState().isHigh()) {
                    lastHigh = System.currentTimeMillis();
                }
            }

            listeners.stream().forEach((listener) -> {
                listener.changedEvent(event.getState().isLow());
            });
        });
    }

    @Override
    public void addListener(SwitchListener listener) {
        listeners.add(listener);
    }

    @Override
    public long getLastPressionMillisec() {
        if (lastLow == 0) {
            return -1L;
        }

        synchronized (this) {
            return lastHigh - lastLow;
        }
    }

    @Override
    public boolean isPushed() {
        return button.getState().isLow();
    }

    @Override
    public void removeListener(SwitchListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void shutdown() {
        listeners.clear();
    }

}
