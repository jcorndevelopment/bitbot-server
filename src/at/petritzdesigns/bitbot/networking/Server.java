package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.control.pi2go.Pi2GoRoboter;
import at.petritzdesigns.bitbot.exceptions.BitBotException;
import at.petritzdesigns.bitbot.networking.data.ServerPort;
import java.util.Scanner;

/**
 * The entry point of the server.
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Server {

    public static void main(String[] args) {
        BitBotServer server = null;
        try {
            server = new BitBotTcpServer(new Pi2GoRoboter(), ServerPort.DEFAULT_TCP_PORT.getPort());
            server.start();
            if (server.isRunning()) {
                System.out.println("Server started on port: " + server.getPort());
                Scanner s = new Scanner(System.in);
                for (;;) {
                    if (s.hasNext()) {
                        if (s.next().equals("stop")) {
                            server.stop();
                            System.exit(0);
                        }
                    }
                }
            } else {
                throw new BitBotException("Server could not be started.");
            }
        } catch (Exception e) {
            System.err.println(e);
        } finally {
            try {
                if (server != null) {
                    server.stop();
                }
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

}
